class Player{
  constructor(){
    this.name = "Phil";
    //lists of units and buildings
    this.units = [];
    this.buildings = [];
    //race
    this.race = "Goblin";
    //wood from chopping down trees
    this.wood = 100;
    //food, each race gets thier food from something different
    this.food = 130;
    //crystals, might just be for goblins (could be renamed to special)
    this.crystals = 75;
  }
}
