var screen = document.getElementById('screen');
var gui = document.getElementById('gui');

var miniMap = document.createElement("CANVAS");

//background music
var vid = document.getElementById("music");
vid.volume = volume;
//var miniMapData;
//stuff for making units selectable
//empty array for holding all of the units on screen
var units = [];
var selected = 0;
//TODO this comes with the class so all the logic to do with this can be swapped
var selectedType = 0;

function getPixels(){
  screen.height = screenHeight;
  screen.width = screenWidth;
}

//stuff for keyboard input
window.onkeydown = function(e){
  let key = e.keyCode;
  switch(key){
    case 81://Q
      console.log("game stopped");
      stop();
      break;
    case 16://shift
      keys.shift = true;
      break;
    case 37://left arrow
      e.preventDefault();
      keys.left = true;
      break;
    case 38://up arrow
      e.preventDefault();
      keys.up = true;
      break;
    case 39://right arrow
      e.preventDefault();
      keys.right = true;
      break;
    case 40://down arrow
      e.preventDefault();
      keys.down = true;
      break;
    case 80://p
      placing = !placing;
      break;
    case 84://t for tile
      selectedTile++;
      if(selectedTile >= tiles.length){
        selectedTile = 0;
      }
      break;
    case 89://y (because it is next to t)
      selectedTile--;
      if(selectedTile < 0){
        selectedTile = tiles.length - 1;
      }
      break;
    case 187://+ key for zooming in
      if(zoom > MIN_ZOOM){
        setZoom(zoom /= ZOOM_AMOUNT);
      }
      break;
    case 189://- key for zooming out
      if(zoom < MAX_ZOOM){
        setZoom(zoom *= ZOOM_AMOUNT);
      }
      break;
    case 48://0 because it's next to the other zoom keys
      //set zoom back to normal
      setZoom(1);
      break;
    default:
      //if the key pressed doesn't do anything else
      console.log(key);
      break;
  }
}

window.onkeyup = function(e){
  let key = e.keyCode;
  switch(key){
    case 16://shift
      keys.shift = false;
      break;
    case 37:
      keys.left = false;
      break;
    case 38:
      keys.up = false;
      break;
    case 39:
      keys.right = false;
      break;
    case 40:
      keys.down = false;
      break;
    default:
      //if the key unpressed doesn't do anything else
      break;
  }
}
//mouse input
//TODO deal with this properly
screen.onmousedown = function(e){
  //if the left click button was pressed
  console.log(e.which);
  if(e.which == 1){
    mouseDown = true;
    if(placing && mouseTile != false){
      game.map.map[mouseTile.x][mouseTile.y].id = selectedTile;
    }
    else{// if(game.buildingMask[mouseTile.x][mouseTile.y] != 0){
      let newSelection = false;
      //see if a unit has been clicked on
      for(let i = 0; i < units.length; i++){
        let u = units[i];
        //TODO make a function and replace this code with it
        let coords = getXY(u.x, u.y);
        //let x = coords.x + u.imWidth/2 - shiftX;
        let x = coords.x + TILE_WIDTH/2 - u.imWidth/2 - shiftX;
        let y = coords.y - u.imHeight + TILE_HEIGHT/2 - shiftY;
        if(mouseCoords.x >= x && mouseCoords.y >= y && mouseCoords.x <= x + u.imWidth
        && mouseCoords.y <= y + u.imHeight){
          selected = u;
          selectedType = "unit";
          //finish function if selected
          newSelection = true;
          break;
        }
      }
      if(!newSelection){
        selected = game.buildingMask[mouseTile.x][mouseTile.y];
        if(game.buildingMask[mouseTile.x][mouseTile.y] != 0){
          selectedType = "building";
        }
        else{
          selectedType = 0;
        }
      }
    }
  }
  else if(e.which == 3){
    if(selectedType == "unit" && game.buildingMask[mouseTile.x][mouseTile.y] == 0){
      game.path(selected,mouseTile.x,mouseTile.y);
    }
  }
}

//window on mouse up not screen so the game knows if they take the mouse up
//outside the window
window.onmouseup = function(e){
  //make sure the left button was unclicked
  if(e.which == 1){
    mouseDown = false;
  }
}

screen.onmouseout = function(e){
}

screen.onmousemove = function(e){
  let x = e.clientX - screen.getBoundingClientRect().left;
  let y = e.clientY - screen.getBoundingClientRect().top;
  mouseCoords.x = x * zoom;
  mouseCoords.y = y * zoom;
  //sort of the tile which the mouse is over
  let t = getTile(mouseCoords.x + shiftX, mouseCoords.y +shiftY);
  if(t != false){
    mouseTile = {"x": Math.floor(t.x - 0.5), "y": Math.floor(t.y + 0.5)};
  }
  else{
    mouseTile = false;
  }
  if(mouseTile != false && placing && mouseDown){
    game.map.map[mouseTile.x][mouseTile.y].id = selectedTile;
  }
}

//function to draw the screen
function redraw(){
  if(screen.getContext){
    let context = screen.getContext("2d");
    //fill background
    drawBackground(context);
    //draw buildings after
    drawEntities(context);
  }
}

function drawBackground(context){
  context.fillStyle = BACKGROUND;
  context.fillRect(0, 0, screenWidth, screenHeight);

  let background = game.map.getBackground();
  //draw this one tile at a time
  for(let i = 0; i < background.length; i++){
    //get the real coords
    let coords = getXY(background[i].x, background[i].y);
    context.drawImage(tiles[background[i].id] ,coords.x - shiftX,coords.y-shiftY);
    //if the mouse is over this tile let the user know
    if(mouseTile.x == background[i].x && mouseTile.y == background[i].y){
      if(placing){
        context.drawImage(tiles[selectedTile] ,coords.x - shiftX,coords.y-shiftY);
      }
      else if (true) {
        context.drawImage(hover ,coords.x - shiftX,coords.y-shiftY);
      }
    }
  }
}

function drawEntities(context){
  //TODO don't draw offscreen-boys
  let entities = game.getEntities();
  units =  entities.units;
  let objects = entities.objects;

  for(let i = 0; i < objects.length; i++){
    let e = objects[i];
    let coords = getXY(e.x, e.y);
    //if e is a building
    if(e.type == 1){
      context.drawImage(e.image, coords.x - shiftX, coords.y - (e.imHeight-(1+((e.length-1)/2))*TILE_HEIGHT) - shiftY);
    }
    //if e is a monster
    else{
      context.drawImage(e.image, coords.x + TILE_WIDTH/2 - e.imWidth/2 - shiftX, coords.y - e.imHeight + TILE_HEIGHT/2 - shiftY);
    }
  }
}
//*******************************
//zooming
function setZoom(z){
  zoom = z;
  screenWidth = defualtWidth * zoom;
  screenHeight = defualtHeight * zoom;
  getPixels();
}

//get the move speed
function getMoveSpeed(){
  let speed = MOVE_SPEED;
  speed *= zoom;
  if(keys.shift){
    speed *= 2;
  }
  //otherwise just return move speed
  return speed;
}


//*******************************
//for looping the game
function run(){
  running = true;
  //this will be a game object eventually
  game = new Game();
  loop();
}
//for stopping the game, this might get quite big
function stop(){
  running = false;
}

function loop(){
  setTimeout(function () {
    if(keys.up){
      shiftY -= getMoveSpeed();
    }
    if(keys.down){
      shiftY += getMoveSpeed();
    }
    if(keys.left){
      shiftX -= getMoveSpeed();
    }
    if(keys.right){
      shiftX += getMoveSpeed();
    }
    //step the game
    game.step();
    //draw the screen again
    redraw();
    drawGUI();
    if(running){
      loop();
    }
  }, RUN_SPEED);
}
//code for th gui etc.
function drawGUI(){
  if(gui.getContext){
    let context = gui.getContext("2d");
    context.fillStyle = GUI_BACKGROUND;
    context.fillRect(0, 0, screenWidth, screenHeight);
    //mini map
    //draws a box
    context.lineWidth = 2;
    context.beginPath();
    context.fillStyle = MAP_FILL;
    context.rect(guiWidth - 140, 10, 130, 130);
    context.fill();
    context.strokeStyle = BORDER_COLOR;
    context.rect(guiWidth - 140, 10, 130, 130);
    context.stroke();
    //makeMiniMap(context);
    drawMiniMap(context);
    //draw a rectangle around where the screen would be
    context.beginPath();
    context.strokeStyle = "#ff0";
    //1.3 wasn't a magic number after all, it was 130/100 aka miniMapSize/WIDTH
    let yRatio = 130/mapHeight;
    let xRatio = 130/mapWidth;
    context.rect(guiWidth - 140 + ((shiftX*xRatio)/(TILE_WIDTH)), 10 + ((shiftY*yRatio)/TILE_HEIGHT), xRatio*screenWidth/TILE_WIDTH, yRatio*screenHeight/TILE_HEIGHT);
    context.stroke();
    //put the info about the player in the top left
    //TODO use context.measureText() to make sure the writing doesn't overlap stuff
    context.font = "20px Dark Wedding";
    context.textBaseline = 'top';
    context.fillStyle = "#000";
    context.fillText(game.players[0].name + " The " + game.players[0].race, 10,10);
    //put in resources
    context.drawImage(goblinFood, 10, 30);
    context.fillText(game.players[0].food, 50, 40);
    context.drawImage(goblinWood, 10, 70);
    context.fillText(game.players[0].wood, 50, 80);
    context.drawImage(goblinCrystals, 10, 110);
    context.fillText(game.players[0].crystals, 50, 120);

    //draw a line to separate stuff
    context.strokeStyle = BORDER_COLOR;
    context.lineWidth=2;
    context.beginPath();
    context.moveTo(200,0);
    context.lineTo(200,gui.height);
    context.stroke();

    if(selected != 0){
      //info about the thing selected
      context.fillText(selected.name, 220,10);
      //draw health before healthBar image
      context.lineWidth = 6;
      context.beginPath();
      context.strokeStyle = "#f00";
      context.moveTo(211,42);
      context.lineTo((115*selected.health/selected.maxHealth)+212, 42);
      context.stroke();
      context.drawImage(healthBar, 205, 30);
      context.fillText(selected.health + "/" + selected.maxHealth, 340, 33)
    }
  }
}

function makeMiniMap(){
  //looks a bit ugly for now but can be changed in the future
  //let newCanvas = document.createElement("CANVAS");
  miniMap.height = mapHeight;
  miniMap.width = mapWidth;
  let mmContext = miniMap.getContext("2d");
  let imageData = mmContext.createImageData(miniMap.width, miniMap.height, 0, 0);

  for(let i = 0; i < game.map.map.length; i++){
    for(let j = 0; j < game.map.map[0].length; j++){
      let cell = (i * game.map.map.length + j) * 4;
      let t = tileData[game.map.map[i][j].id];
      //get the miniMap colour from the tile data
      imageDataRGB(imageData, cell, t.r, t.g, t.b);
    }
  }

  mmContext.putImageData(imageData, 0, 0);
}

//TODO make it so units and buildings show up on here too
function drawMiniMap(context){
  let scaleX = 130/(mapWidth*ROOT_TWO);
  let scaleY = 130/(mapHeight*ROOT_TWO);
  let angle = -45*Math.PI/180;
  //x and y positions
  let x = (guiWidth - 140)/scaleX;
  let y = 75/scaleY;
  //rotated x and y
  let rotX = (x*Math.cos(angle) + y * Math.sin(angle));
  let rotY = -(x*Math.sin(angle) - y * Math.cos(angle));
  context.scale(scaleY, scaleX);
  //rotate miniMap
  context.rotate(angle);
  context.drawImage(miniMap, rotX, rotY);//(guiWidth - 140)/scaleX, 10/scaleY);
  //back to normal
  context.scale(1/scaleY,1/scaleX);
  context.rotate(-angle);
}

function imageDataRGB(imageData, cell, r, g, b){
  imageData.data[cell] = r;
  imageData.data[cell + 1] = g;
  imageData.data[cell + 2] = b;
  imageData.data[cell + 3] = 255;
}
/*main game stuff*/
//something goes wrong if I don't do this (also includes set pixels)
setZoom(1);
run();
//set the pixels in the GUI, this shouldn't to be changed so I'm doing it here
//after the game has started running so all the info can be drawn
gui.height = guiHeight;
gui.width = guiWidth;
makeMiniMap();
//needs calling twice, I have no idea why
drawGUI();
setTimeout(drawGUI, 5);
