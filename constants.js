//game background colour
const BACKGROUND = "#333";
const GUI_BACKGROUND = "#EEC";
const MAP_FILL = "#840";
const BORDER_COLOR = "#420";
//the size of a tile
const TILE_WIDTH = 64;
const TILE_HEIGHT = 32;
//frames per second
const FPS = 30;
const RUN_SPEED = 1000/FPS;
//speed the arrow keys move you arround the map
const MOVE_SPEED = 18;
//for zooming
const ZOOM_AMOUNT = 1.2;
const MAX_ZOOM = 4;
const MIN_ZOOM = 0.25;

//for maths
const ROOT_TWO = 1.4142135623730951;
