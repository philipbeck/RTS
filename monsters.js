class Monster{
  constructor(){
    this.type = 0;
    this.name = "Defualt-boy";
    this.maxHealth = 100;
    this.health = 100;
    this.attack = 3;
    this.range = 0;//0 range means melee attack
    this.x = 50;
    this.y = 50;
    this.speed = 0.5;
    this.path = [];
    //this will be used for all kinds of things, such as animation and healing
    this.age = 0;
  }

  step(){
    //increment age before anything else happens
    this.age++;
    //if the unit is currently pathing
    if(this.path.length > 0){
      //desination for the next bit of pathing
      let dest = this.path[this.path.length-1];
      let distance = {"x":dest.x-this.x, "y": dest.y-this.y};
      //if the monster is nearer than one step from it's next desination just go there
      if((Math.pow(distance.x, 2) + Math.pow(distance.y, 2)) < Math.pow(this.speed,2)){
        this.x = dest.x;
        this.y = dest.y;
        this.path.pop();
      }
      else{
        let l = Math.sqrt((Math.pow(distance.x, 2) + Math.pow(distance.y, 2)));
        this.x += this.speed * (distance.x/l);
        this.y += this.speed * (distance.y/l);
      }
    }
  }

  //TODO this can be massively optimised if it's slowing the program down
  //TODO so many bugs but it's kind of ok
  getPath(x,y,map,buildings){
    //clear the old path
    this.path = [];
    let thisX = Math.floor(this.x + 0.5);
    let thisY = Math.floor(this.y + 0.5);
    //array for searching
    let path = [];
    for(let i = 0; i < map.length; i++){
      //make the path array into a two dimensional one
      path[i] = [];
    }
    //fitness needs working out properly
    let unlocked = [];
    //put the first element into path and unlocked
    path[thisX][thisY] = {"checked": false, "locked": false, "x": this.x,"y": this.y, "distTo": distance(thisX, thisY, x, y), "distFrom": 0}
    unlocked.push(path[thisX][thisY]);
    //variable to show if the pathing is finished
    let finished = false;
    //can the shape be improved?
    pathLoop:
    while(unlocked.length > 0){
      //find the element with the best fitness value
      let square = unlocked[0];
      for(let i = 1; i < unlocked.length; i++){
        if(getFitness(square) < getFitness(unlocked[i])){
          square = unlocked[i];
        }
        else if(getFitness(square) == getFitness(unlocked[i]) && square.distTo > unlocked[i].distTo){
          square = unlocked[i];
        }
      }

      for(let a = -1; a < 2; a += 2){
        for(let b = 0; b < 2; b ++){
          console.log("a")
          //this part is just a hacky fix for my bad code
          let i;
          let j;
          if(b == 0){
            i = a;
            j = 0;
          }
          else {
            j = a;
            i = 0;
          }
          if((square.x + i) >= 0 && (square.y + j) >= 0 && (square.y + j) < map.length
            && (square.x + i) < map.length){
            if(square.x + i == x && square.y + j == y){
              //distance is just set to huge because everything goes wierd if you
              //let the computer work it out
              path[square.x + i][square.y + j] = {"checked": true, "locked": false, "x": square.x + i,"y": square.y + j, "distTo": 0, "distFrom": 9999999999};
              finished = true;
              //finish searching and go to the next stage
              break pathLoop;
            }
            let p = path[square.x + i][square.y + j];
            if(typeof p == 'undefined'){
              //if there is something in the way
              if(buildings[square.x + i][square.y + j] != 0){
                //big numbers so no one has to see it
                path[square.x + i][square.y + j] = {"checked": true, "locked": true, "x": square.x + i,"y": square.y + j, "distTo": 99999 + distance(square.x + i, square.y + j, x, y), "distFrom": 99999 + distance(square.x + i, square.y + j, thisX, thisY)};
                //continue;
              }
              else{
                path[square.x + i][square.y + j] = {"checked": false, "locked": false, "x": square.x + i,"y": square.y + j, "distTo": distance(square.x + i, square.y + j, x, y), "distFrom": square.distFrom + 1};
                unlocked.push(path[square.x + i][square.y + j]);
              }
              //console.log(path[square.x + i][square.y + j]);
            }
            else if(!p.locked && square.distTo + 1 < p.distTo){
                p.distTo = square.distTo + 1;
            }
          }
        }
      }
      //once a place has been checked lock it
      square.locked = true;
      unlocked.splice(unlocked.indexOf(square),1);
    }

    this.path.push({"x": x, "y": y});
    //uses finished instead of true which means if wont try to make a path if
    //one wasn't found
    pathLoop2:
    while(finished && !(x == thisX && y == thisY)){
      let current = path[x][y]
      //see if something changed
      let changed = false;
      current.checked = true;
      for(let a = -1; a < 2; a += 2){
        for(let b = 0; b < 2; b++){
          //this part is just a hacky fix for my bad code
          let i;
          let j;
          if(b == 0){
            j = a;
            i = 0;
          }
          else {
            j = 0;
            i = a;
          }
          let p = path[x + i][y + j];
          if(typeof p != 'undefined' && !p.checked && p.distFrom <= current.distFrom){
            p.checked;
            //dirty shallow clone
            current = JSON.parse(JSON.stringify(p));
            changed = true;
          }
        }
      }
      if(!changed){
        console.log("something went wrong " + x + " " + y);
        break pathLoop2;
      }
      x = current.x;
      y = current.y;
      this.path.push({"x": x, "y": y})
    }
    return finished;
  }

}

//some really small functions to save me typing so much
function distance(tX, tY, eX, eY){
  return Math.abs(eX-tX) + Math.abs(eY-tY);
}

function getFitness(square){
  return -(square.distTo + square.distFrom);
}



//other classes
class Goblin extends Monster{
  constructor(x, y){
    super();

    this.name = "Goblin";
    //not sure how this will look
    this.image = goblin;
    this.imHeight = 64;
    this.imWidth = 32;

    this.maxHealth = 100;
    this.health = 100;
    this.attack = 3;
    this.range = 0;//0 range means melee attack
    this.x = x;
    this.y = y;
    this.speed = 0.12;
    //this.path = []//{"x":2,"y":4},{"x": 5, "y": 10},{"x": 20, "y": 20}, {"x": 10, "y": 10}];//for moving
  }

  step(){
    super.step();
    //TODO make this more universal
    switch(this.age%20){
      case 0:
        this.image = goblin;
        break;
      case 10:
        this.image = goblin2;
        break;
    }
  }
}

class Goblet extends Monster{
  constructor(x, y){
    super();

    this.name = "Goblet";
    //not sure how this will look
    this.image = goblet;
    this.imHeight = 48;
    this.imWidth = 24;

    this.maxHealth = 50;
    this.health = 50;
    this.attack = 2;
    this.range = 0;//0 range means melee attack
    this.x = x;
    this.y = y;
    this.speed = 0.15;
    //this.path = []//{"x":2,"y":4},{"x": 5, "y": 10},{"x": 20, "y": 20}, {"x": 10, "y": 10}];//for moving
  }
}

//this was useful for fixing the pathing so i'll leave it until the pathing deffinitely works
/*
    //*******************************************************************************************
    let mapString = "";
    for(let i = 0; i < 100; i++){
      for(let j = 0; j < 100; j++){
        if(buildings[i][j] != 0){
          mapString += "b ";
        }
        else if(typeof(path[i][j]) == "undefined"){
          mapString += ". ";
        }
        else{
          mapString += path[i][j].distFrom;
          if(path[i][j].distFrom < 10){
            mapString += " ";
          }
        }
      }
      mapString += "\n";
    }
    console.log(mapString);
    //*******************************************************************************************
*/
