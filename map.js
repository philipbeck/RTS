class Map{
  //this will take in criteria at some point
  constructor(){
    //make and fill up the map
    //map is layed out like this: map[topleft][bottomright]
    this.map = [];
    for(let i = 0; i < mapWidth; i++){
      this.map[i] = []
      for(let j = 0; j < mapHeight; j++){
        this.map[i][j] = new Tile(1,i,j);
      }
    }
  }

  //TODO this is horribly hardcoded, make it data driven
  //TODO get that seeded function for math.random so the a seed gived the exact same map
  generateMap(game){
    let s;//simplex varible
    let r;//random varible
    for(let i = 0; i < mapWidth; i++){
      for(let j = 0; j < mapHeight; j++){
        s = noise.simplex2(i*biomeFocus,j*biomeFocus);
        if(s < -0.4){
          s = noise.simplex2(i*terrainFocus,j*terrainFocus);
          if(s < -0.5){
            this.map[i][j] = new Tile(6,i,j);//lava
          }
          else if(s < -0.45){
            this.map[i][j] = new Tile(17,i,j);//bakedEarth
          }
          else{
            let r = Math.random();
            if(r > 0.1){
              this.map[i][j] = new Tile(4,i,j);//lavaRock
              if(r > 0.99){
                let t = new Tree(i,j);
                t.image = deadTree;
                game.addBuilding(t);
              }
            }
            else{
              this.map[i][j] = new Tile(5,i,j);//crystals
            }
          }
        }
        else if(s < 0.4){
          //plus 50 so a different section of noise is seen
          s = noise.simplex2(i*terrainFocus + 50,j*terrainFocus + 50);
          if(s < -0.5){
            this.map[i][j] = new Tile(3,i,j);//deepWater
          }
          else if(s < -0.3){
            this.map[i][j] = new Tile(2,i,j);//water
          }
          else if(s < 0.5){
            r = Math.random();
            if(r < 0.05){
              this.map[i][j] = new Tile(19,i,j);//flowerGrass
            }
            else{
              this.map[i][j] = new Tile(1,i,j);//grass
            }
          }
          else{
            this.map[i][j] = new Tile(13,i,j);//leaves
            let t = new Tree(i,j);
            r = Math.random();
            if(r < 0.15){
              t.image = snowyTree;
            }
            //add a tree sometimes but not always so the forrest doesn't look too
            //cramped
            if(r < 0.6){
              game.addBuilding(t);
            }
          }
        }
        else{
          s = noise.simplex2(i*terrainFocus + 100,j*terrainFocus + 100);
          if(s < -0.9){
            this.map[i][j] = new Tile(6,i,j);//lava
          }
          else if(s < 0.3){
            r = Math.random();
            if(r > 0.9){
              this.map[i][j] = new Tile(11,i,j);//snowyRock
            }
            else{
              this.map[i][j] = new Tile(10,i,j);//rock
              if(r < 0.01){
                let t = new Tree(i,j);
                t.image = snowyTree;
                game.addBuilding(t);
              }
            }
          }
          else if(s < 0.4){
            this.map[i][j] = new Tile(11,i,j);//snowyRock
          }
          else if(s < 0.9){
            this.map[i][j] = new Tile(7,i,j);//snow
          }
          else{
            this.map[i][j] = new Tile(6,i,j);//lava
          }
        }
      }
    }
  }

  getBackground(){
    let background = [];
    //new background maths (well hard)
    let i = 0;
    let j = 0;
    //TODO this goes throught twice as many tiles as it should do so could be
    //improved quite a lot
    for(i = (shiftX - (shiftX%TILE_WIDTH) - TILE_WIDTH); i < (shiftX + screenWidth); i += TILE_WIDTH/2){
      for(j = (shiftY - (shiftY%TILE_HEIGHT) - TILE_HEIGHT); j < (shiftY + screenHeight); j += TILE_HEIGHT/2){
        let t = getTile(i,j);
        if(t != false &&t.x%1 == 0 && t.y%1 == 0){
          //console.log(i+ " " + j + "\n" + t.x + " " + t.y + "\n" + (i + j*2)%64 + "\n" + (i + j*2)%32);
          background.push(this.map[t.x][t.y]);
        }
      }
    }
    return background;
  }
}

//this is important for drawing tiles
//takes in the index of a tile and says where it will be on the screen
function getXY(indexX, indexY){
  if(indexX >= mapWidth || indexY >= mapHeight){
    //the tile must be on the map
    return false;
  }

  let x = (indexX+indexY)*TILE_WIDTH/2;
  let y = (mapHeight-indexY+indexX)*TILE_HEIGHT/2;

  return {"x": x, "y": y};
}

//this gives slightly wrong values
//to fix subtract 0.5 from x and add 0.5 to y
//TODO fix existing code to use the correct values
function getTile(mapX,mapY){
  //some horrible maths to turn co-ordinates into an index
  let a = (mapX)/TILE_WIDTH;
  let b = ((mapY)/TILE_HEIGHT) - mapHeight/2;
  //simultaneus equations, yuck
  let x = a+b;
  let y = a-b;
  if(x < 0 || x >= mapWidth || y < 0 || y >= mapHeight){
    return false;
  }
  return {"x": x, "y": y};
}

//********TILES****************************************
//TODO not sure if this is needed
class Tile{
  constructor(tileId,x,y){
    this.id = tileId;
    this.x = x;
    this.y = y;
  }
}
